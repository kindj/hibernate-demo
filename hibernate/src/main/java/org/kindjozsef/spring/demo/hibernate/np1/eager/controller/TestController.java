package org.kindjozsef.spring.demo.hibernate.np1.eager.controller;

import org.kindjozsef.spring.demo.hibernate.np1.eager.model.PostComment;
import org.kindjozsef.spring.demo.hibernate.np1.eager.service.PostCommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private PostCommentRepository postCommentRepository;

    @GetMapping(path = "/testPCRepositoryAttributeAccess")
    public String testPCRepositoryAttributeAccess() {
        for(PostComment pc : postCommentRepository.findAll()) {
            System.out.println(pc.getPost().getTitle());
        }
        return "testPCRepositoryAttributeAccess";
    }

    @GetMapping(path = "/testPCRepositoryOnlyMethodCall")
    public String testPCRepositoryOnlyMethodCall() {
        postCommentRepository.findAll();
        return "testPCRepositoryOnlyMethodCall";
    }

}
