package org.kindjozsef.spring.demo.hibernate.np1.lazy.service;

import org.kindjozsef.spring.demo.hibernate.np1.lazy.model.PostComment2;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCommentRepository2 extends CrudRepository<PostComment2, Long>, PostCommentRepositoryCustom {
}
