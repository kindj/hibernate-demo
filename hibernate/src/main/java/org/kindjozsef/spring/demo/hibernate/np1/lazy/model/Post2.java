package org.kindjozsef.spring.demo.hibernate.np1.lazy.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Post2")
@Table(name = "post2")
public class Post2 {

    @Id
    private Long id;
    private String title;

    public Post2(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Post2() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
