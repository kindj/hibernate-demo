package org.kindjozsef.spring.demo.hibernate.np1.lazy.service;

import org.kindjozsef.spring.demo.hibernate.np1.lazy.model.PostComment2;

import java.util.List;

public interface PostCommentRepositoryCustom {

    List<PostComment2> getPostCommentDeep();

}
