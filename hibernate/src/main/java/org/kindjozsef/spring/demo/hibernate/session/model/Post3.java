package org.kindjozsef.spring.demo.hibernate.session.model;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Post3")
@Table(name = "post3")
public class Post3 {

    @Id
    private Long id;
    private String title;

    public Post3(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Post3() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
