package org.kindjozsef.spring.demo.hibernate.session.service;

import org.kindjozsef.spring.demo.hibernate.np1.lazy.model.PostComment2;
import org.kindjozsef.spring.demo.hibernate.session.model.Post3;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository3 extends CrudRepository<Post3, Long>, PostRepositoryCustom {
}
