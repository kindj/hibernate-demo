INSERT INTO post (title, id) VALUES ('High-Performance Java Persistence - Part 1', 1)
INSERT INTO post (title, id) VALUES ('High-Performance Java Persistence - Part 2', 2)
INSERT INTO post (title, id) VALUES ('High-Performance Java Persistence - Part 3', 3)
INSERT INTO post (title, id) VALUES ('High-Performance Java Persistence - Part 4', 4)

INSERT INTO post_comment (post_id, review, id) VALUES (1, 'Excellent book to understand Java Persistence', 1)
INSERT INTO post_comment (post_id, review, id) VALUES (2, 'Must-read for Java developers', 2)
INSERT INTO post_comment (post_id, review, id) VALUES (3, 'Five Stars', 3)
INSERT INTO post_comment (post_id, review, id) VALUES (4, 'A great reference book', 4)

INSERT INTO post2 (title, id) VALUES ('High-Performance Java Persistence - Part 1', 1)
INSERT INTO post2 (title, id) VALUES ('High-Performance Java Persistence - Part 2', 2)
INSERT INTO post2 (title, id) VALUES ('High-Performance Java Persistence - Part 3', 3)
INSERT INTO post2 (title, id) VALUES ('High-Performance Java Persistence - Part 4', 4)

INSERT INTO post_comment2 (post_id, review, id) VALUES (1, 'Excellent book to understand Java Persistence', 1)
INSERT INTO post_comment2 (post_id, review, id) VALUES (2, 'Must-read for Java developers', 2)
INSERT INTO post_comment2 (post_id, review, id) VALUES (3, 'Five Stars', 3)
INSERT INTO post_comment2 (post_id, review, id) VALUES (4, 'A great reference book', 4)


INSERT INTO post3 (title, id) VALUES ('High-Performance Java Persistence - Part 1', 1)
INSERT INTO post3 (title, id) VALUES ('High-Performance Java Persistence - Part 2', 2)
INSERT INTO post3 (title, id) VALUES ('High-Performance Java Persistence - Part 3', 3)
INSERT INTO post3 (title, id) VALUES ('High-Performance Java Persistence - Part 4', 4)