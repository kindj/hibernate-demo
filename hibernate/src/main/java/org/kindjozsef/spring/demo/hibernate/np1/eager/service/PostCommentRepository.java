package org.kindjozsef.spring.demo.hibernate.np1.eager.service;

import org.kindjozsef.spring.demo.hibernate.np1.eager.model.PostComment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCommentRepository extends CrudRepository<PostComment, Long> {
}
