package org.kindjozsef.spring.demo.hibernate.np1.lazy.model;


import javax.persistence.*;

@Entity(name = "PostComment2")
@Table(name = "post_comment2")
public class PostComment2 {

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Post2 post;

    private String review;

    public PostComment2(Long id, Post2 post, String review) {
        this.id = id;
        this.post = post;
        this.review = review;
    }

    public PostComment2() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Post2 getPost() {
        return post;
    }

    public void setPost(Post2 post) {
        this.post = post;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
