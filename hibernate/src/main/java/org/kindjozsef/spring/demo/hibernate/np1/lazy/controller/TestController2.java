package org.kindjozsef.spring.demo.hibernate.np1.lazy.controller;

import org.kindjozsef.spring.demo.hibernate.np1.lazy.model.PostComment2;
import org.kindjozsef.spring.demo.hibernate.np1.lazy.service.PostCommentRepository2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/np1/lazy")
public class TestController2 {

    @Autowired
    private PostCommentRepository2 postCommentRepository;

    @GetMapping(path = "/testPCRepositoryAttributeAccess")
    public String testPCRepositoryAttributeAccess() {
        for(PostComment2 pc : postCommentRepository.findAll()) {
            System.out.println(pc.getPost().getTitle());
        }
        return "testPCRepositoryAttributeAccess";
    }

    @GetMapping(path = "/testPCRepositoryOnlyMethodCall")
    public String testPCRepositoryOnlyMethodCall() {
        postCommentRepository.findAll();
        return "testPCRepositoryOnlyMethodCall";
    }

}
