package org.kindjozsef.spring.demo.hibernate.session.controller;

import org.hibernate.SessionFactory;
import org.kindjozsef.spring.demo.hibernate.np1.lazy.model.PostComment2;
import org.kindjozsef.spring.demo.hibernate.np1.lazy.service.PostCommentRepository2;
import org.kindjozsef.spring.demo.hibernate.session.model.Post3;
import org.kindjozsef.spring.demo.hibernate.session.service.PostRepository3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.UUID;

@RestController
@RequestMapping(path = "/session")
public class TestController4 {


    @Autowired
    private PostRepository3 postRepository;

    private Random random = new Random();

    private boolean end = true;

    @GetMapping(path = "/end")
    public String end() {
        this.end = false;
        sleep(10000);
        this.end = true;
        return "end";
    }

    //Test it and see the logs, but basically it means that:
    //spring.jpa.open-in-view=true
    //  the session of hibernate is opened here e.g. in the testSessionAccess method and we use the First-Level cache
    //spring.jpa.open-in-view=false
    //  the session of hibernate is opened in the transactionalService(Repository in this fall).
    @GetMapping(path = "/sessionAccess")
    public String testSessionAccess() {
        UUID uuid = UUID.randomUUID();
        do {
            System.out.println(uuid.toString() + "sessionAccess Id 1 = " + postRepository.findById(1L).get().getTitle());
            sleep(5000);
            System.out.println(uuid.toString() + "sessionAccess Id 2 = " + postRepository.findById(2L).get().getTitle());
            String r = random.nextInt(10) + "";
            System.out.println("Random is " + r);
            Post3 post31L = postRepository.findById(1L).get();
            post31L.setTitle(r);
            Post3 post32L = postRepository.findById(2L).get();
            post32L.setTitle(r);
            postRepository.save(post32L);

        } while(end);
        return "sessionAccess";
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}