package org.kindjozsef.spring.demo.hibernate.np1.lazy.service;

import org.kindjozsef.spring.demo.hibernate.np1.lazy.model.PostComment2;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class PostCommentRepositoryCustomImpl implements PostCommentRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PostComment2> getPostCommentDeep() {
        return entityManager.createQuery("select pc from PostComment2 " +
                "pc join fetch pc.post p", PostComment2.class)
                        .getResultList();
    }
}
